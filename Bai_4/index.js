/**
 * Input
 * Cho người dùng nhập vào chiều dài chiều rộng
 * 
 * Todo
 * Diện tích = chiều dài * chiều rộng
 * Chu vi = (chiều dài + chiều rông) * 2
 * 
 * output
 * In ra màn hình kết quả
 * 
 * 
 */

function tinh() {
    var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
    var chieuRong = document.getElementById("txt-chieu-rong").value * 1;

    chuVi = (chieuDai + chieuRong) * 2;
    dienTich = chieuDai * chieuRong;

    document.getElementById("ketQua").innerHTML = `Chu vi: ${chuVi} ; Diện tích: ${dienTich}`;

}