/**
 * Input
 * Cho người dùng nhập vào 1 số thực có 2 chữ số
 * 
 * Todo
 * Công thức tính:
 * hangChuc = Math.floor(n / 10) % 10;
 * hangDonVi = n % 10;
 * ketQua = hangDonVi + hangChuc;
 * 
 * Output
 * In kết quả ra màn hình
 * 
 */

function tinhTong() {
    var tong2KiSo = document.getElementById("txt-tong-2-ki-so").value * 1;
    hangChuc = Math.floor(tong2KiSo / 10) % 10;
    hangDonVi = tong2KiSo % 10;

    ketQua = hangDonVi + hangChuc;

    document.getElementById("ketQua").innerHTML = `Kết quả là: ${ketQua}`;
}





